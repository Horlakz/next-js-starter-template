import { useRouter } from "next/router";
import { FC, useEffect } from "react";

import { Storage } from "../utilties/storage";

// Props type
interface Props {
  children: any;
}

const AuthProtect: FC<Props> = ({ children }) => {
  const router = useRouter();
  const checkToken = new Storage().check("token");

  useEffect(() => {
    if (!checkToken) {
      // redirect to login
      router.push("/account/login");
    }
  }, [checkToken, router]);

  return <>{children}</>;
};
export default AuthProtect;
