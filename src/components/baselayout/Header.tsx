import Link from "next/link";
import Image from "next/image";

import { MdMessage, MdPerson, MdLogout } from "react-icons/md";

import Storage from "../../utilities/storage";

const Header = (): JSX.Element => {
  const isAuth = new Storage().check("token");

  return (
    <header className="flex items-center justify-between md:px-28 px-4 py-4 shadow-md">
      <Link href="/">
        <Image src="/img/logo.jpg" width={60} height={50} />
      </Link>

      <nav className="flex-center gap-16">
        {isAuth ? (
          <ul className="flex md:gap-6 gap-4 text-sm md:text-lg">
            <Link href="/account">
              <li className="font-bold flex-center gap-1 cursor-pointer">
                <MdPerson size={20} />
                <span>My Dashboard</span>
              </li>
            </Link>
            <Link href="/logout">
              <li className="flex-center gap-1 cursor-pointer">
                <MdLogout size={20} />
                <span>Logout</span>
              </li>
            </Link>
          </ul>
        ) : (
          <ul className="flex md:gap-6 gap-4 text-sm md:text-lg">
            <li>
              <Link href="/login">Login</Link>
            </li>
            <li>
              <Link href="/register">SignUp</Link>
            </li>
          </ul>
        )}

        <Link href="/contact-us">
          <button className="hidden md:flex center gap-1 px-3 py-1.5 text-white bg-blue-700 hover:bg-blue-600 default-transition rounded-lg">
            <MdMessage />
            <span>Contact Us</span>
          </button>
        </Link>
      </nav>
    </header>
  );
};

export default Header;
