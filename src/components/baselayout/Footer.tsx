import Link from "next/link";

const Footer = () => {
  return (
    <footer className="bg-blue-900 text-blue-50 p-4 md:p-10 grid center gap-1.5 md:gap-4">
      <p>
        &copy; <Link href="/">Kiriku</Link>
        &nbsp;{new Date().getFullYear()}
      </p>

      <ul className="flex gap-4 divide-x">
        <Link href="/contact-us" as="/contact-us">
          <li className="pl-4 cursor-pointer">Contact Us</li>
        </Link>
        <Link href="/about" as="/about">
          <li className="pl-4 cursor-pointer">About</li>
        </Link>
        <Link href="/faq" as="/faq">
          <li className="pl-4 cursor-pointer">FAQ</li>
        </Link>
      </ul>
    </footer>
  );
};

export default Footer;
