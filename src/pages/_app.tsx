import type { AppProps } from 'next/app'
import "../styles/globals.scss";
import "react-toastify/dist/ReactToastify.css";
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";
import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import { ToastContainer, Slide } from "react-toastify";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QueryClientProvider client={new QueryClient()}>
      <Component {...pageProps} />
      <ToastContainer position="top-center" transition={Slide} />
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

export default MyApp;
