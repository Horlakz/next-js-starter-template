import axios, { type AxiosInstance } from "axios";

const { NEXT_PUBLIC_API_URL } = process.env;

axios.defaults.baseURL = NEXT_PUBLIC_API_URL;
axios.defaults.xsrfHeaderName = "X-CSRFToken";
axios.defaults.responseEncoding = "utf8";

export const client: AxiosInstance = axios.create({});
